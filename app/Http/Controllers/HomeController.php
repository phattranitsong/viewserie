<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function search(Request $request)
    {
        $search = $request->get('search');
        $viewseries = DB::table('viewseries')->where('title','like','%'.$search.'%')->paginate(6);
//         $viewseries = DB::table('viewseries')->where('name','like','%'.$search.'%')->paginate(6);
        return view('search',['title' =>  $viewseries],compact('viewseries'));
    }

    public function getType()
    {
        $viewseries = DB::table('viewseries')->pluck("type","id");
        return view('dropdown',compact('viewseries'));
    }
}
