<?php

namespace App\Http\Controllers;

use App\Viewserie;
use App\User;
use Illuminate\Http\Request;

class ViewseriesController extends Controller
{
    public  function  __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $viewseries = Viewserie::OrderBy('rate','desc')->get();

        return view('index')->with('viewseries',$viewseries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
            [
                'title'=>'required',
                'content'=>'required',
                'rate'=>'required',
                'creator'=>'required',
                'star'=>'required',
                'type'=>'required',
                'file'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
                'video'=>'required|mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040'
            ]
        );

        //       dd($request->file());
        $file_name = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'),$file_name);

        $video_name = time().'.'.$request->video->extension();
        $request->video->move(public_path('uploads'),$video_name);

        $viewserie = new Viewserie();
        $viewserie->title = $request->input('title');
        $viewserie->content = $request->input('content');
        $viewserie->rate = $request->input('rate');
        $viewserie->creator = $request->input('creator');
        $viewserie->star = $request->input('star');
        $viewserie->type = $request->input('type');
        $viewserie->file_name = $file_name;
        $viewserie->video_name = $video_name;
        $viewserie->save();

        return redirect('/')->with('success','ลงฐานข้อมมูลแล้วจ้า');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $viewserie = Viewserie::find($id);
        return view('show')->with('viewserie',$viewserie);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $viewserie = Viewserie::find($id);
        return view('edit')->with('viewserie',$viewserie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $viewserie = Viewserie::find($id);
        $viewserie->title = $request->input('title');
        $viewserie->content = $request->input('content');
        $viewserie->rate = $request->input('rate');
        $viewserie->save();
        return redirect('/')
            ->with('success','Edit Success ')
            ->with('editAgain',$viewserie->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $viewserie = Viewserie::find($id);
        $viewserie->delete();
        return redirect('/')->with('success','Deleted Success '.'000'.$viewserie->id);

    }
}
