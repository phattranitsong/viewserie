<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware'=>['web']],function(){
    Route::get('/','ViewseriesController@index'); // เรียก url แบบที่ 1

    Route::resource('viewserie','ViewseriesController'); // เรียก url แบบที่ 2

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/search','HomeController@search');

    Route::get('dropdownlist/getstates/{id}','DataController@getStates');

});
