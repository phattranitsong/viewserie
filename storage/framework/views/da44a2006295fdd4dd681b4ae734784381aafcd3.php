<?php $__env->startSection('content'); ?>

    <style>
        body{
            background: black;
            color: white;
        }
    </style>
    <nav>
        <form class="form-inline my-2 my-lg-0"  method="get" action="<?php echo e(url("/search")); ?>">

            <a href="<?php echo e(url('/')); ?>"><img src="<?php echo asset('asset/logovs.png'); ?>" style="width:20%;"></a>
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="  SEARCH" aria-label="Search" style="border:none;border-radius:30px;padding: 5px 15px;margin-left:650px;position:absolute;z-index: auto;">
            

            <select class="custom-select" style="border:none;border-radius:30px;float: right;width:130px;height: 40px;position:absolute;z-index: auto;margin-left: 850px;">
                <option>ALL</option>
                <option>DRAMA</option>
                <option>COMEDY</option>
                <option>FANTASY</option>
                <option>ROMANCE</option>
                <option>ACTION</option>
            </select>

            <a href="<?php echo e(url('/viewserie/create/')); ?>" class="btn" style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin-left: 1000px;position:absolute;z-index: auto;">Add New Series</a>


        </form>

    </nav>

    <div class="container" style="margin:  0 auto;">
    <form method="post" action="<?php echo e(url('/viewserie/')); ?>" enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="<?php echo e(old('title')); ?>">
        </div>
        <div class="form-group">
            <label>Content</label>
            <input type="text"  name="content" class="form-control" value="<?php echo e(old('content')); ?>">
        </div>
        <div class="form-group">
            <label>Rate</label>
            <input type="text" name="rate" class="form-control" value="<?php echo e(old('rate')); ?>">
        </div>

        <div class="form-group">
            <label>Directed</label>
            <input type="text" name="creator" class="form-control" value="<?php echo e(old('creator')); ?>">
        </div>
        <div class="form-group">
            <label>Star</label>
            <input type="text"  name="star" class="form-control" value="<?php echo e(old('star')); ?>">
        </div>
        <div class="form-group">
            <label>Genres</label>
            <input type="text" name="type" class="form-control" value="<?php echo e(old('type')); ?>">
        </div>

        <div class="form-group">
            <label>Poster</label>
            <input type="file" name="file" class="form-control">
        </div>

        <div class="form-group">
            <label>Trailer</label>
            <input type="file" name="video" class="form-control">
        </div>
        <button type="submit" class="btn" style="border:none;border-radius:30px;background: #384FA2;color: white;float: right;">submit</button>
    </form>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/viewseries/resources/views/create.blade.php ENDPATH**/ ?>