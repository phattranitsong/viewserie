<?php $__env->startSection('content'); ?>
    <nav>


        <form class="form-inline my-2 my-lg-0"  method="get" action="<?php echo e(url("/search")); ?>">

            <a href="<?php echo e(url('/')); ?>"><img src="<?php echo asset('asset/logovs.png'); ?>" style="width:20%;margin:20px 0px;"></a>
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="  SEARCH" aria-label="Search" style="border:none;border-radius:30px;padding: 5px 15px;float: right;margin: 40px 450px;">
            


            <select class="custom-select" style="border:none;border-radius:30px;float: right;width: 130px;height: 40px;">
                <option>ALL</option>
                <option>DRAMA</option>
                <option>COMEDY</option>
                <option>FANTASY</option>
                <option>ROMANCE</option>
                <option>ACTION</option>
            </select>

            <a href="<?php echo e(url('/viewserie/create/')); ?>" class="btn" style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin-left: 10px;">Add New Series</a>


        </form>

    </nav>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <?php if(session('status')): ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo e(session('status')); ?>

                            </div>
                        <?php endif; ?>




                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/viewseries/resources/views/home.blade.php ENDPATH**/ ?>