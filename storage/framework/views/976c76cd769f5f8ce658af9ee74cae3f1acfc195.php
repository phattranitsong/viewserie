<?php $__env->startSection('content'); ?>

    <style>
        body{
            background: black;
            color: white;
            /*font-family: Montserrat;*/

        }
        .box1{
            width: 250px;
            height: 350px;
            background: white;
            margin: 10px 0;

        }
        .box2{
            width: 550px;
            height: 350px;
            /*background: white;*/
            margin: 10px 0 10px 0;
            margin-left: -55px;
        }
        .box3{
            width: 350px;
            height: 250px;
            background: gray;
            margin: 10px 0;
        }
        .nopadding {
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>
    <nav>
        <form class="form-inline my-2 my-lg-0"  method="get" action="<?php echo e(url("/search"), false); ?>">

            <a href="<?php echo e(url('/'), false); ?>"><img src="<?php echo asset('asset/logovs.png'); ?>" style="width:20%;"></a>
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="  SEARCH" aria-label="Search" style="border:none;border-radius:30px;padding: 5px 15px;margin-left:650px;position:absolute;z-index: auto;">
            

            <select class="custom-select" style="border:none;border-radius:30px;float: right;width:130px;height: 40px;position:absolute;z-index: auto;margin-left: 850px;">
                <option>ALL</option>
                <option>DRAMA</option>
                <option>COMEDY</option>
                <option>FANTASY</option>
                <option>ROMANCE</option>
                <option>ACTION</option>
            </select>

            <a href="<?php echo e(url('/viewserie/create/'), false); ?>" class="btn" style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin-left: 1000px;position:absolute;z-index: auto;">Add New Series</a>


        </form>

    </nav>
    <br>

    <div class="container-fluid">
        <div class="row nopadding">

            <div class="col nopadding">

                <div class="box1 ">
                    <img src="<?php echo e(url('/uploads/' .$viewserie->file_name), false); ?>"width="250">
                    <div style="background:#384FA2;margin-top: -35px;position: absolute;z-index:1;width:250px;">
                        <p style="margin:5px 15px 0px 15px;"><i class="fas fa-star" style="color:#FFC700;"></i> <?php echo e($viewserie->rate, false); ?></p>
                        <b style="margin:0px 15px 5px 15px;"><?php echo e($viewserie->title, false); ?></b>
                    </div>
                    <div>
                        <p style="margin:30px 15px 0px 15px;">Director : <?php echo e($viewserie->creator, false); ?></p>
                        <b style="margin:5px 15px 5px 15px;"> Starr : <?php echo e($viewserie->star, false); ?></b>
                    </div>
                    <div>
                        <p style="margin:10px 15px 0px 15px;">ID : <?php echo e($viewserie->id, false); ?></p>
                        <b style="margin:5px 15px 5px 15px;"> Create  : <?php echo e($viewserie->created_at, false); ?></b>
                    </div>

                </div>
            </div>

            <div class="col-5 nopadding">
                <div class="box2">

                    <video width="550" height="350" controls>
                        <source src="<?php echo e(url('/uploads/' .$viewserie->video_name), false); ?>" type="video/mp4">
                    </video>
                </div>
                <div style="margin-top:20px;margin-left: -40px;width: 500px;">
                    <p><?php echo e($viewserie->content, false); ?></p>
                </div>
            </div>


            <div class="col nopadding">
                <h3>Recommend for you</h3>
                <br>

                <div class="box3">













                </div>
            </div>

        </div>

    </div>







    <form action="<?php echo e(url('/viewserie/'.$viewserie->id), false); ?>" method="post" id="form-delete" style="margin-top: 150px;">
        <?php echo method_field('DELETE'); ?>
        <?php echo csrf_field(); ?>
        
        <button class="btn btn-danger" onclick="confirm_delete()" type="button">Delete</button>
        <a href="<?php echo e(url('/viewserie/'.$viewserie->id.'/edit'), false); ?>" class="btn" style="border:none;background: #384FA2;color: white;margin-left: 10px;">Edit</a>

    </form>

    <script>
        function confirm_delete() {
            var text = '<?php echo $viewserie->title; ?>';
            //ต้องใส่ !! เพราะไม่แน่ใจว่ามันเป็น textรึปล่า
            var confirm = window.confirm('ยืนยันการลบ'+text);
            if (confirm){
                document.getElementById('form-delete').submit();
            }
        }

    </script>

    <script src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- example_responsive_1 -->
    <ins class="adsbygoogle example_responsive_1"
         style="display:inline-block"
         data-ad-client="ca-pub-XXXXXXX11XXX9"
         data-ad-slot="8XXXXX1"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/viewseries/resources/views/show.blade.php ENDPATH**/ ?>