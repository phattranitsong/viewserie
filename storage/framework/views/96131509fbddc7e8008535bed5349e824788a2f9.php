<?php $__env->startSection('content'); ?>

    <nav>
        <form class="form-inline my-2 my-lg-0"  method="get" action="<?php echo e(url("/search")); ?>">

            <a href="<?php echo e(url('/')); ?>"><img src="<?php echo asset('asset/logovs.png'); ?>" style="width:20%;"></a>
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="  SEARCH" aria-label="Search" style="border:none;border-radius:30px;padding: 5px 15px;margin-left:650px;position:absolute;z-index: auto;">
            

            <select class="custom-select" style="border:none;border-radius:30px;float: right;width:130px;height: 40px;position:absolute;z-index: auto;margin-left: 850px;">
                <option>ALL</option>
                <option>DRAMA</option>
                <option>COMEDY</option>
                <option>FANTASY</option>
                <option>ROMANCE</option>
                <option>ACTION</option>
            </select>

            <a href="<?php echo e(url('/viewserie/create/')); ?>" class="btn" style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin-left: 1000px;position:absolute;z-index: auto;">Add New Series</a>


        </form>

    </nav>
    <form method="post" action="<?php echo e(url('/viewserie/'.$viewserie->id)); ?>">
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <br>
        <div class="container">

            <div class="form-group">
                <label style="color:white">Title</label>
                <input type="text" name="title" class="form-control" value="<?php echo e($viewserie->title); ?>">
            </div>
            <div class="form-group">
                <label style="color:white">Content</label>
                <input type="text"  name="content" class="form-control" value="<?php echo e($viewserie->content); ?>">
            </div>
            <div class="form-group">
                <label style="color:white">Rate</label>
                <input type="text" name="rate" class="form-control" value="<?php echo e($viewserie->rate); ?>">
            </div>
            <button type="submit"  style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin: 0 auto;">submit</button>

        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/viewseries/resources/views/edit.blade.php ENDPATH**/ ?>