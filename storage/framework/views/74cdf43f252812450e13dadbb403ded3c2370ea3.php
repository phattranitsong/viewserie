<?php $__env->startSection('content'); ?>
    <style>
        .box3{
            width: 350px;
            height: 250px;
            background: gray;
            margin: 10px 0;
        }
        .nopadding {
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>


    <nav>
        <form class="form-inline my-2 my-lg-0"  method="get" action="<?php echo e(url("/search"), false); ?>">

            <a href="<?php echo e(url('/'), false); ?>"><img src="<?php echo asset('asset/logovs.png'); ?>" style="width:20%;"></a>
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="  SEARCH" aria-label="Search" style="border:none;border-radius:30px;padding: 5px 15px;margin-left:650px;position:absolute;z-index: auto;">


            <select class="custom-select" id="restrictCategory" style="border:none;border-radius:30px;float: right;width:130px;height: 40px;position:absolute;z-index: auto;margin-left: 850px;">
                <option>ALL</option>
                <option>DRAMA</option>
                <option>COMEDY</option>
                <option>FANTASY</option>
                <option>ROMANCE</option>
                <option>ACTION</option>
            </select>











            <a href="<?php echo e(url('/viewserie/create/'), false); ?>" class="btn" style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin-left: 1000px;position:absolute;z-index: auto;">Add New Series</a>


        </form>

    </nav>



    <b><h3 style="color:#384FA2;">TOP 5 RATING SERIES</h3></b>

    <?php echo csrf_field(); ?>

    <div class="container-fluid">
        <div class="row">

            <?php $count = 0; ?>

            <?php if(count($viewseries) > 0): ?>


                <?php $__currentLoopData = $viewseries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $viewserie): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <?php if($count == 5) break; ?>


                    

                    <a href="<?php echo e(url('/viewserie/'.$viewserie->id), false); ?>">

                        <div class="box1" style="margin-left: 20px;">
                            <img src="<?php echo e(url('/uploads/' .$viewserie->file_name), false); ?>"width="250" height="350">
                            <div style="background:#384FA2;margin-top: -35px;position: absolute;z-index: inherit;width:250px;">
                                <p style="margin:1px 15px 0px 5px;padding: 5px;"><i class="fas fa-star" style="color:#FFC700;"></i> <?php echo e($viewserie->rate, false); ?></p>
                                <b style="margin:0px 15px 5px 15px;padding: 5px;"><?php echo e($viewserie->title, false); ?></b>
                            </div>
                        </div>

                        <?php $count++; ?>



        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php endif; ?>

                    </a>
             </div>


            <b><h3 style="color:#384FA2;margin-top: 50px;">DRAMA</h3></b>

        <div class="row">


        <?php if(count($viewseries) > 0): ?>
            <?php $__currentLoopData = $viewseries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $viewserie): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



                    <a href="<?php echo e(url('/viewserie/'.$viewserie->id), false); ?>">
                    <div class="col">

                        <video width=300" height="200" controls>
                            <source src="<?php echo e(url('/uploads/' .$viewserie->video_name), false); ?>" type="video/mp4">
                        </video>
                        <div style="background:white;margin-top: -35px;position: absolute;z-index: inherit;width:300px;color:#384FA2 ">
                             <b style="margin:0px 15px 5px 15px;padding: 5px;"><?php echo e($viewserie->title, false); ?>   <i class="fas fa-star" style="color:#FFC700;"></i> <?php echo e($viewserie->rate, false); ?></b>

                         </div>
                    </div>


            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

                    </a>
                </div>






        <h2 style="text-align: center;color: white;margin-top: 10px;"><?php echo e('มีซีรีส์ทั้งหมด : ' .count($viewseries).' เรื่อง', false); ?></h2>

    </div>


    <script type="text/javascript">
        jQuery(document).ready(function ()
        {
            jQuery('select[name="country"]').on('change',function(){
                var countryID = jQuery(this).val();
                if(countryID)
                {
                    jQuery.ajax({
                        url : 'dropdownlist/getstates/' +countryID,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            jQuery('select[name="state"]').empty();
                            jQuery.each(data, function(key,value){
                                $('select[name="state"]').append('<option value="'+ key +'">'+ value +'</option>');
                            });
                        }
                    });
                }
                else
                {
                    $('select[name="state"]').empty();
                }
            });
        });
    </script>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/viewseries/resources/views/index.blade.php ENDPATH**/ ?>