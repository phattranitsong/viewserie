{{--{{create.blade.php}}--}}
@extends('layouts.app')
@section('content')

    <nav>
        <form class="form-inline my-2 my-lg-0"  method="get" action="{{url("/search")}}">

            <a href="{{ url('/') }}"><img src="<?php echo asset('asset/logovs.png'); ?>" style="width:20%;"></a>
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="  SEARCH" aria-label="Search" style="border:none;border-radius:30px;padding: 5px 15px;margin-left:650px;position:absolute;z-index: auto;">
            {{--            <i class="fas fa-search" style="position: absolute;z-index:1;color: black;margin-left: 140px;padding-left: 10px"></i>--}}

            <select class="custom-select" style="border:none;border-radius:30px;float: right;width:130px;height: 40px;position:absolute;z-index: auto;margin-left: 850px;">
                <option>ALL</option>
                <option>DRAMA</option>
                <option>COMEDY</option>
                <option>FANTASY</option>
                <option>ROMANCE</option>
                <option>ACTION</option>
            </select>

            <a href="{{url('/viewserie/create/')}}" class="btn" style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin-left: 1000px;position:absolute;z-index: auto;">Add New Series</a>


        </form>

    </nav>
    <form method="post" action="{{ url('/viewserie/'.$viewserie->id) }}">
        @csrf
        @method('PUT')
        <br>
        <div class="container">

            <div class="form-group">
                <label style="color:white">Title</label>
                <input type="text" name="title" class="form-control" value="{{$viewserie->title}}">
            </div>
            <div class="form-group">
                <label style="color:white">Content</label>
                <input type="text"  name="content" class="form-control" value="{{$viewserie->content}}">
            </div>
            <div class="form-group">
                <label style="color:white">Rate</label>
                <input type="text" name="rate" class="form-control" value="{{$viewserie->rate}}">
            </div>
            <button type="submit"  style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin: 0 auto;">submit</button>

        </div>
    </form>
@endsection
