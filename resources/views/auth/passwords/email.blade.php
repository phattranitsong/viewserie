{{--{{index.blade.php}}--}}
@extends('layouts.app')
@section('content')
    <h1>All Post</h1>
    <a href="{{url('/todo/create/')}}" class="btn btn-primary">New Post</a>
    <a href="{{url('/todo/')}}" class="btn btn-info">View all post</a>
    @csrf

    @if(count($todos) > 0)
        @foreach($todos as $todo)

            <div>
                <a href="{{url('/todo/'.$todo->id)}}">

                    <h3>{{$todo->title}}</h3>
                </a>
                <p>{{$todo->created_at}}</p>
                <p>{{$todo->content}}</p>


                <p>{{$todo->due}}</p>
                <img src="{{ url('/uploads/' .$todo->file_name) }}" width="120">
            </div>
            <hr>
        @endforeach

    @endif



@endsection

