{{--{{search.blade.php}}--}}
@extends('layouts.app')
@section('content')

    @csrf
    <style>
        body{
            font-family: Kanit;
            {{--background-image: url({{URL::asset('img/bg.png')}});--}}
            background-image: url("/img/bg.jpg");
            /*background-image:url('../public/css/image/bg.png');*/
        }
        .cover {
            object-fit: cover;
        }
    </style>

    <nav>
        <form class="form-inline my-2 my-lg-0"  method="get" action="{{url("/search")}}">

            <a href="{{ url('/') }}"><img src="<?php echo asset('asset/logovs.png'); ?>" style="width:20%;"></a>
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="  SEARCH" aria-label="Search" style="border:none;border-radius:30px;padding: 5px 15px;margin-left:650px;position:absolute;z-index: auto;">
            {{--            <i class="fas fa-search" style="position: absolute;z-index:1;color: black;margin-left: 140px;padding-left: 10px"></i>--}}

            <select class="custom-select" style="border:none;border-radius:30px;float: right;width:130px;height: 40px;position:absolute;z-index: auto;margin-left: 850px;">
                <option>ALL</option>
                <option>DRAMA</option>
                <option>COMEDY</option>
                <option>FANTASY</option>
                <option>ROMANCE</option>
                <option>ACTION</option>
            </select>

            <a href="{{url('/viewserie/create/')}}" class="btn" style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin-left: 1000px;position:absolute;z-index: auto;">Add New Series</a>


        </form>

    </nav>

    <br>



    <div class="container-fluid">
        <div class="row">

            @if(count($viewseries) > 0)
                @foreach($viewseries as $viewserie)


                    {{--        <a href="{{url('/tutor/'.$tutor->id)}}">--}}

                    <a href="{{url('/viewserie/'.$viewserie->id)}}">

                        <div class="box1" style="margin-left: 20px;">
                            <img src="{{ url('/uploads/' .$viewserie->file_name) }}"width="250">
                            <div style="background:#384FA2;margin-top: -35px;position: absolute;z-index: inherit;width:250px;">
                                <p style="margin:1px 15px 0px 5px;padding: 5px;"><i class="fas fa-star" style="color:#FFC700;"></i> {{$viewserie->rate}}</p>
                                <b style="margin:0px 15px 5px 15px;padding: 5px;">{{$viewserie->title}}</b>
                            </div>
                        </div>

                 @endforeach

                @else
                            <div style="margin: 0 auto;">
                                <h2 style="color: white;text-align: center;">ไม่พบข้อมูล</h2>
                            </div>
            @endif

        </div>
    </div>
