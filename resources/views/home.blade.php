@extends('layouts.app')

@section('content')
    <nav>


        <form class="form-inline my-2 my-lg-0"  method="get" action="{{url("/search")}}">

            <a href="{{ url('/') }}"><img src="<?php echo asset('asset/logovs.png'); ?>" style="width:20%;margin:20px 0px;"></a>
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="  SEARCH" aria-label="Search" style="border:none;border-radius:30px;padding: 5px 15px;float: right;margin: 40px 450px;">
            {{--            <i class="fas fa-search" style="position: absolute;z-index:1;color: black;margin-left: 140px;padding-left: 10px"></i>--}}


            <select class="custom-select" style="border:none;border-radius:30px;float: right;width: 130px;height: 40px;">
                <option>ALL</option>
                <option>DRAMA</option>
                <option>COMEDY</option>
                <option>FANTASY</option>
                <option>ROMANCE</option>
                <option>ACTION</option>
            </select>

            <a href="{{url('/viewserie/create/')}}" class="btn" style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin-left: 10px;">Add New Series</a>


        </form>

    </nav>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif




                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
