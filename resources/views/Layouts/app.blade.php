<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Viewseries</title>
</head>
<style>
    body{
        background: black;
        /*font-family: Montserrat;*/
        font-family: supermarket;
    }
    a:hover{
        color: white;
    }
</style>
<body>
<div class="container-fluid" style="margin: 20px;">
    @include('inc.message')
    @if(Auth::check())

        @if(Auth::user()->rule == 'admin')
            <div style="color: #f66d9b"> {{ Auth::user()->email . "(". Auth::user()->rule .")" }}</div>
            <form action="{{url('/logout')}}" method="post">
                @csrf
                <button>logout</button>
            </form>

        @endif

        @if(Auth::user()->rule == 'member')
            <div style="color: #6cb2eb"> {{ Auth::user()->email . "(". Auth::user()->rule .")" }}</div>
            <form action="{{url('/logout')}}" method="post">
                @csrf
                <button>logout</button>


            </form>

        @endif
    @endif


    @yield('content')
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/65757397fd.js" crossorigin="anonymous"></script>

</body>
</html>
