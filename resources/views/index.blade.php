{{--{{index.blade.php}}--}}
@extends('layouts.app')
@section('content')
    <style>
        .box3{
            width: 350px;
            height: 250px;
            background: gray;
            margin: 10px 0;
        }
        .nopadding {
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>


    <nav>
        <form class="form-inline my-2 my-lg-0"  method="get" action="{{url("/search")}}">

            <a href="{{ url('/') }}"><img src="<?php echo asset('asset/logovs.png'); ?>" style="width:20%;"></a>
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="  SEARCH" aria-label="Search" style="border:none;border-radius:30px;padding: 5px 15px;margin-left:650px;position:absolute;z-index: auto;">
{{--            <i class="fas fa-search" style="position: absolute;z-index:1;color: black;margin-left: 140px;padding-left: 10px"></i>--}}

            <select class="custom-select" id="restrictCategory" style="border:none;border-radius:30px;float: right;width:130px;height: 40px;position:absolute;z-index: auto;margin-left: 850px;">
                <option>ALL</option>
                <option>DRAMA</option>
                <option>COMEDY</option>
                <option>FANTASY</option>
                <option>ROMANCE</option>
                <option>ACTION</option>
            </select>


{{--            <div class="form-group">--}}
{{--                <label for="restrictCategory">Choose your website categories restrictions</label><br>--}}
{{--                <select class="custom-select" id="restrictCategory" multiple>--}}
{{--                    <option name="test4" value="test4">Option 1</option>--}}
{{--                    <option name="test5" value="test5">Option 2</option>--}}
{{--                    <option name="test6" value="test6">Option 3</option>--}}
{{--                </select>--}}
{{--            </div>--}}

            <a href="{{url('/viewserie/create/')}}" class="btn" style="border:none;border-radius:30px; padding:10px 15px;background: #384FA2;color: white;margin-left: 1000px;position:absolute;z-index: auto;">Add New Series</a>


        </form>

    </nav>



    <b><h3 style="color:#384FA2;">TOP 5 RATING SERIES</h3></b>
{{--    <a href="{{url('/viewserie/')}}" class="btn btn-info">View all post</a>--}}
    @csrf

    <div class="container-fluid">
        <div class="row">

            <?php $count = 0; ?>

            @if(count($viewseries) > 0)


                @foreach($viewseries as $viewserie)

                    <?php if($count == 5) break; ?>


                    {{--        <a href="{{url('/tutor/'.$tutor->id)}}">--}}

                    <a href="{{url('/viewserie/'.$viewserie->id)}}">

                        <div class="box1" style="margin-left: 20px;">
                            <img src="{{ url('/uploads/' .$viewserie->file_name) }}"width="250" height="350">
                            <div style="background:#384FA2;margin-top: -35px;position: absolute;z-index: inherit;width:250px;">
                                <p style="margin:1px 15px 0px 5px;padding: 5px;"><i class="fas fa-star" style="color:#FFC700;"></i> {{$viewserie->rate}}</p>
                                <b style="margin:0px 15px 5px 15px;padding: 5px;">{{$viewserie->title}}</b>
                            </div>
                        </div>

                        <?php $count++; ?>



        @endforeach

    @endif

                    </a>
             </div>


            <b><h3 style="color:#384FA2;margin-top: 50px;">DRAMA</h3></b>

        <div class="row">


        @if(count($viewseries) > 0)
            @foreach($viewseries as $viewserie)



                    <a href="{{url('/viewserie/'.$viewserie->id)}}">
                    <div class="col">

                        <video width=300" height="200" controls>
                            <source src="{{url('/uploads/' .$viewserie->video_name) }}" type="video/mp4">
                        </video>
                        <div style="background:white;margin-top: -35px;position: absolute;z-index: inherit;width:300px;color:#384FA2 ">
                             <b style="margin:0px 15px 5px 15px;padding: 5px;">{{$viewserie->title}}   <i class="fas fa-star" style="color:#FFC700;"></i> {{$viewserie->rate}}</b>

                         </div>
                    </div>


            @endforeach
        @endif

                    </a>
                </div>




{{--        @foreach ($viewseries as $viewserie)--}}

        <h2 style="text-align: center;color: white;margin-top: 10px;">{{ 'มีซีรีส์ทั้งหมด : ' .count($viewseries).' เรื่อง'}}</h2>
{{--        @endforeach--}}
    </div>


    <script type="text/javascript">
        jQuery(document).ready(function ()
        {
            jQuery('select[name="country"]').on('change',function(){
                var countryID = jQuery(this).val();
                if(countryID)
                {
                    jQuery.ajax({
                        url : 'dropdownlist/getstates/' +countryID,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            jQuery('select[name="state"]').empty();
                            jQuery.each(data, function(key,value){
                                $('select[name="state"]').append('<option value="'+ key +'">'+ value +'</option>');
                            });
                        }
                    });
                }
                else
                {
                    $('select[name="state"]').empty();
                }
            });
        });
    </script>

@endsection


